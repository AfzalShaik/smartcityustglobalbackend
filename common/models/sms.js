'use strict';

module.exports = function (Sms) {

  Sms.observe('before save', function (ctx, next) {
    var utf8 = require('utf8');
    ctx.instance.message = ctx.instance.message.toString().replace(/ /g, "%20");

    var http = require('http');
    // var options = {
    //   host: 'www.smslogin.mobi',
    //   path: '/spanelv2/api.php?username=telepathy&password=telepathy&to='+ctx.instance.destination+'&from=TLPATY&message='+ctx.instance.message
    // };

    var des=ctx.instance.dest.join();
    console.log("destination"+des);
    var options = {
      host: 'smslogin.mobi',
      path: '/spanelv2/api.php?username=smartcity&password=smartcity&from=SMTCTY&to=' + des + '&message='+ctx.instance.message
    };

    var req = http.request(options, function (res) {
      res.setEncoding('utf8');
      res.on('data', function (chunk) {
        console.log('Response: ' + chunk);
      });
    });

    req.end();
    //your logic goes here
    next();
  });

};

