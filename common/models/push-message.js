'use strict';

module.exports = function(Pushmessage) {
    Pushmessage.observe('before save', function(ctx, next){
        if(ctx.isNewInstance){
            Pushmessage.find({"where":{"description":ctx.instance.description}}, function(err, resp) {
              console.log(err, resp);
                if(err) next(err, null);
                if(resp.length!= undefined && resp.length>=1){
                    // let errrror = "null";
                    next(null, null);
                }else{next();}

            });
        }else{ next();}

    });
  //update Pushmessage remoteMethod
  Pushmessage.updateIncident = function(input, cb) {
      Pushmessage.findOne({'where': {'id' : input.id}}, function(findErr, findOut) {
        if (findOut) {
          var obj = findOut;
          obj.read = 'Yes';
            findOut.updateAttributes(obj, function(updateErr, updateOut) {
              console.log('updateeeeeeeeeeeeeee ', updateErr, updateOut);
              cb(null, updateOut);
            });
        } else {
          cb(null, null);
        }
      });
  }
  Pushmessage.remoteMethod('updateIncident', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/updateIncident',
      verb: 'PUT',
    },
  });
  // post push messages
  Pushmessage.postIncident = function(input, callBC) {
    Pushmessage.find({"where":{"description":input.description}}, function(err, resp) {
      if (resp.length > 0) {
        callBC(null, null);
      } else {
        var obj = input;
        Pushmessage.create(obj, function(postErr, postOut) {
          callBC(null, postOut);
        });
      }
    });
  }
  Pushmessage.remoteMethod('postIncident', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/postIncident',
      verb: 'POST',
    },
  });
};
